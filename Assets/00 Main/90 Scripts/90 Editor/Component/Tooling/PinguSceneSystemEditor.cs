using _00_Main._90_Scripts._00_Runtime.Components.Tooling;
using PcSoft.UnityScene._90_Scripts._90_Editor.Components;
using UnityEditor;
using UnityEngine;

namespace _00_Main._90_Scripts._90_Editor.Component.Tooling
{
    [CustomEditor(typeof(PinguSceneSystem))]
    public sealed class PinguSceneSystemEditor : SceneSystemEditor<PinguScene>
    {
        private SerializedProperty _menuMusicProperty;
        
        protected override void OnEnable()
        {
            base.OnEnable();
            _menuMusicProperty = serializedObject.FindProperty("menuMusic");
        }

        protected override void OnCustomGUI()
        {
            EditorGUILayout.Space();
            LabeledArea("Audio", () =>
            {
                EditorGUILayout.PropertyField(_menuMusicProperty, new GUIContent("Menu Music"));
            });
        }
    }
}