using System.Text.RegularExpressions;
using UnityEngine;

namespace _00_Main._90_Scripts._90_Editor.Utils
{
    public static class FontReader
    {
        private static readonly Regex FontRegex = new Regex("\\(glyph \\(unicode ([0-9]+)\\) \\(offset (-?[0-9]+) (-?[0-9]+)\\) \\(advance ([0-9]+)\\) \\(rect ([0-9]+) ([0-9]+) ([0-9]+) ([0-9]+)\\)\\)");

        public static FontData? Read(string line)
        {
            var match = FontRegex.Match(line);
            if (!match.Success)
                return null;

            var unicode = int.Parse(match.Groups[1].Value);
            var offsetX = int.Parse(match.Groups[2].Value);
            var offsetY = int.Parse(match.Groups[3].Value);
            var advance = int.Parse(match.Groups[4].Value);
            var rectX = int.Parse(match.Groups[5].Value);
            var rectY = int.Parse(match.Groups[6].Value);
            var rectMaxX = int.Parse(match.Groups[7].Value);
            var rectMaxY = int.Parse(match.Groups[8].Value);

            return new FontData(unicode, new Vector2(offsetX, offsetY), advance, Rect.MinMaxRect(rectX, rectY, rectMaxX, rectMaxY));
        }
    }

    public readonly struct FontData
    {
        public int Unicode { get; }
        public Vector2 Offset { get; }
        public int Advance { get; }
        public Rect Rect { get; }

        public FontData(int unicode, Vector2 offset, int advance, Rect rect)
        {
            Unicode = unicode;
            Offset = offset;
            Advance = advance;
            Rect = rect;
        }
    }
}