using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using _00_Main._90_Scripts._90_Editor.Utils;
using UnityEditor;
using UnityEngine;

namespace _00_Main._90_Scripts._90_Editor.Actions
{
    public sealed class FontCalculationWindow : EditorWindow
    {
        [MenuItem("Tools/Pingus/Calculate Font")]
        public static void CalculateFontMenuItem()
        {
            EditorWindow.GetWindow<FontCalculationWindow>(true);
        }

        private string file, font;
        private int width, height;
        private float relativeY = -12f;

        private void OnGUI()
        {
            EditorGUI.BeginDisabledGroup(true);
            file = EditorGUILayout.TextField("Font File", file);
            EditorGUI.EndDisabledGroup();
            if (GUILayout.Button("Choose Font File"))
            {
                file = EditorUtility.OpenFilePanel("Font File", null, "font");
            }
            
            EditorGUILayout.Space();

            width = EditorGUILayout.IntField("Width", width);
            height = EditorGUILayout.IntField("Height", height);
            relativeY = EditorGUILayout.FloatField("Relative Y", relativeY);
            
            EditorGUILayout.Space();

            EditorGUI.BeginDisabledGroup(string.IsNullOrWhiteSpace(file));
            if (GUILayout.Button("Calculate"))
            {
                font = CalculateFont();
            }
            EditorGUI.EndDisabledGroup();
            
            EditorGUILayout.Space();

            EditorGUILayout.TextArea(font,GUILayout.ExpandHeight(true));
        }

        private string CalculateFont()
        {
            var lines = File.ReadAllLines(file);

            var sb = new StringBuilder();
            foreach (var line in lines)
            {
                var fontData = FontReader.Read(line);
                if (fontData == null)
                    continue;

                var rect = fontData.Value.Rect;
                var offset = fontData.Value.Offset;

                sb
                    .Append("  - serializedVersion: 2").AppendLine()
                    .Append("    index: ").Append(fontData.Value.Unicode).AppendLine()
                    .Append("    uv:").AppendLine()
                    .Append("      serializedVersion: 2").AppendLine()
                    .Append("      x: ").Append(rect.x / width).AppendLine()
                    .Append("      y: ").Append((height - rect.y - rect.height) / height).AppendLine()
                    .Append("      width: ").Append(rect.width / width).AppendLine()
                    .Append("      height: ").Append(rect.height / height).AppendLine()
                    .Append("    vert:").AppendLine()
                    .Append("      serializedVersion: 2").AppendLine()
                    .Append("      x: ").Append(offset.x).AppendLine()
                    .Append("      y: ").Append(relativeY - offset.y).AppendLine()
                    .Append("      width: ").Append(rect.width).AppendLine()
                    .Append("      height: ").Append(-rect.height).AppendLine()
                    .Append("    advance: ").Append(fontData.Value.Advance).AppendLine()
                    .Append("    flipped: 0").AppendLine()
                    ;
            }

            return sb.ToString().Replace(",", ".");
        }
    }
}