using System;
using PcSoft.ExtendedEditor._90_Scripts._00_Runtime.Extra;
using UnityEngine;

namespace _00_Main._90_Scripts._00_Runtime.Assets
{
    public abstract class IdentifiableAsset : ScriptableObject
    {
        #region Inspector Data

        [SerializeField]
        [ReadOnly]
        private string identifier;

        #endregion

        #region Properties

        public string Identifier => identifier;

        #endregion
        
        #region Builtin Methods

#if UNITY_EDITOR
        protected virtual void OnValidate()
        {
            if (string.IsNullOrWhiteSpace(identifier))
            {
                identifier = Guid.NewGuid().ToString();
            }
        }
#endif

        #endregion
    }
}