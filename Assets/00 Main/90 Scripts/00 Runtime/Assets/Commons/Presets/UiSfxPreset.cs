using PcSoft.DynamicAssets._90_Scripts._00_Runtime.Loader;
using UnityEngine;

namespace _00_Main._90_Scripts._00_Runtime.Assets.Commons.Presets
{
    [CreateAssetMenu(menuName = PingusConstants.Menu.Assets.Common.PresetMenu + "/UI SFX Preset")]
    public sealed class UiSfxPreset : ScriptableObject
    {
        #region Static Area

        public static UiSfxPreset Singleton => AssetResourcesLoader.Instance.GetAsset<UiSfxPreset>();

        #endregion
        
        #region Inspector Data

        [SerializeField]
        private AudioClip click;

        #endregion

        #region Properties

        public AudioClip Click => click;

        #endregion
    }
}