using System;
using PcSoft.DynamicAssets._90_Scripts._00_Runtime.Loader;
using UnityEngine;

namespace _00_Main._90_Scripts._00_Runtime.Assets.Commons.Presets
{
    [CreateAssetMenu(menuName = PingusConstants.Menu.Assets.Common.PresetMenu + "/Level Point Preset")]
    public sealed class LevelPointPreset : ScriptableObject
    {
        public static LevelPointPreset Singleton => AssetResourcesLoader.Instance.GetAsset<LevelPointPreset>();

        #region Inspector Data

        [SerializeField]
        private Sprite disabled;

        [Space]
        [SerializeField]
        private LevelPointData locked;

        [SerializeField]
        private LevelPointData unlocked;

        [SerializeField]
        private LevelPointData succeeded;

        #endregion

        #region Properties

        public Sprite Disabled => disabled;

        public LevelPointData Locked => locked;

        public LevelPointData Unlocked => unlocked;

        public LevelPointData Succeeded => succeeded;

        #endregion
    }

    [Serializable]
    public sealed class LevelPointData
    {
        #region Inspector Data

        [SerializeField]
        private Sprite regularly;

        [SerializeField]
        private Sprite highlighted;

        #endregion

        #region Properties

        public Sprite Regularly => regularly;

        public Sprite Highlighted => highlighted;

        #endregion
    }
}