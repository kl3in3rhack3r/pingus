using PcSoft.DynamicAssets._90_Scripts._00_Runtime.Loader;
using PcSoft.ExtendedEditor._90_Scripts._00_Runtime.Extra;
using UnityEngine;

namespace _00_Main._90_Scripts._00_Runtime.Assets.World
{
    [CreateAssetMenu(menuName = PingusConstants.Menu.Assets.WorldMenu + "/World")]
    public sealed class WorldAsset : IdentifiableAsset
    {
        #region Static Area

        public static WorldAsset[] All => AssetResourcesLoader.Instance.GetAssets<WorldAsset>();

        #endregion
        
        #region Inspector Data

        [SerializeField]
        private string title;

        [SerializeField]
        private string description;

        [SerializeField]
        private Sprite icon;

        [SerializeField]
        private AudioClip music;

        [SerializeField]
        private bool alwaysUnlocked;

        [Space]
        [Scene]
        [SerializeField]
        private string scene;

        #endregion

        #region Properties

        public string Title => title;

        public string Description => description;

        public Sprite Icon => icon;

        public AudioClip Music => music;

        public bool AlwaysUnlocked => alwaysUnlocked;

        public string Scene => scene;

        #endregion
    }
}