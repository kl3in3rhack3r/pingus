using PcSoft.ExtendedEditor._90_Scripts._00_Runtime.Extra;
using UnityEngine;

namespace _00_Main._90_Scripts._00_Runtime.Assets.World
{
    [CreateAssetMenu(menuName = PingusConstants.Menu.Assets.WorldMenu + "/Level")]
    public sealed class LevelAsset : IdentifiableAsset
    {
        #region Inspector Data

        [Space]
        [SerializeField]
        private string title;

        [SerializeField]
        private string description;

        [SerializeField]
        private bool alwaysUnlocked;

        [Space]
        [Scene]
        [SerializeField]
        private string scene;

        #endregion

        #region Properties

        public string Title => title;

        public string Description => description;

        public bool AlwaysUnlocked => alwaysUnlocked;

        public string Scene => scene;

        #endregion
    }
}