using System;
using PcSoft.UnityBlending._90_Scripts._00_Runtime.Components;
using UnityEngine;

namespace _00_Main._90_Scripts._00_Runtime.Components.Tooling
{
    [AddComponentMenu(PingusConstants.Menu.Components.ToolingMenu + "/Splash System")]
    [DisallowMultipleComponent]
    public sealed class PinguSplashSystem : SplashSystem
    {
        protected override void OnShow(Action onFinished)
        {
            onFinished?.Invoke();
        }

        protected override void OnClose()
        {
            PinguSceneSystem.Singleton.GotoMenu();
        }
    }
}