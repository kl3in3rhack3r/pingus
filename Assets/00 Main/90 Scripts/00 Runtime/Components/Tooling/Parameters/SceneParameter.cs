using PcSoft.ExtendedUnity._90_Scripts._00_Runtime.Components;

namespace _00_Main._90_Scripts._00_Runtime.Components.Tooling.Parameters
{
    public abstract class SceneParameter<T> : SearchingSingletonBehavior<T> where T : SceneParameter<T>
    {
        
    }
}