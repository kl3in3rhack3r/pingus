using _00_Main._90_Scripts._00_Runtime.Assets.World;
using UnityEngine;

namespace _00_Main._90_Scripts._00_Runtime.Components.Tooling.Parameters
{
    [AddComponentMenu(PingusConstants.Menu.Components.Tooling.ParameterMenu + "/World Scene Parameter")]
    [DisallowMultipleComponent]
    public sealed class WorldSceneParameter : SceneParameter<WorldSceneParameter>
    {
        #region Static Area

        public static WorldAsset World
        {
            get => Singleton._world;
            set => Singleton._world = value;
        }

        #endregion
        
        #region Inspector Data

        [SerializeField]
        private WorldAsset fallbackWorld;

        #endregion

        private WorldAsset _world;

        #region Builtin Methods

        private void Awake()
        {
            _world = fallbackWorld;
        }

        #endregion
    }
}