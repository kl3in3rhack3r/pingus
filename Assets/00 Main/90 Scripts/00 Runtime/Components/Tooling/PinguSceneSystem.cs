using System;
using System.Linq;
using _00_Main._90_Scripts._00_Runtime.Assets.World;
using _00_Main._90_Scripts._00_Runtime.Components.Tooling.Parameters;
using PcSoft.AudioMachine._90_Scripts._00_Runtime.Assets.Music;
using PcSoft.AudioMachine._90_Scripts._00_Runtime.Components.Music;
using PcSoft.UnityScene._90_Scripts._00_Runtime.Components;
using UnityEngine;

namespace _00_Main._90_Scripts._00_Runtime.Components.Tooling
{
    [AddComponentMenu(PingusConstants.Menu.Components.ToolingMenu + "/Scene System")]
    [DisallowMultipleComponent]
    [DefaultExecutionOrder(1)]
    public sealed class PinguSceneSystem : SceneSystem<PinguSceneData, PinguScene>
    {
        #region Static Area

        public static PinguSceneSystem Singleton => Resources.FindObjectsOfTypeAll<PinguSceneSystem>()[0];

        #endregion

        #region Inspector Data

        [SerializeField]
        private AudioClip menuMusic;

        #endregion

        public void GotoMenu()
        {
            LoadScene(PinguScene.Menu);
        }

        public void GotoWorld(WorldAsset worldAsset)
        {
            WorldSceneParameter.World = worldAsset;
            LoadScene(PinguScene.World, worldAsset);
        }

        protected override PinguSceneData FindSceneData(PinguScene state, object data)
        {
            switch (state)
            {
                case PinguScene.Splash:
                case PinguScene.Menu:
                case PinguScene.Game:
                    return base.FindSceneData(state, data);
                case PinguScene.World:
                    var worldAsset = (WorldAsset) data ?? WorldSceneParameter.World;

                    var sceneData = base.FindSceneData(state, data);
                    return new PinguSceneData(state, sceneData.Scenes.Append(worldAsset.Scene).ToArray());
                default:
                    throw new NotImplementedException();
            }
        }

        protected override void OnLoadingStarted(PinguScene oldState)
        {
            MusicMachine.Singleton.Stop();
            PointerInputSystem.Singleton.ClearCamera();
        }

        protected override void OnLoadingFinished(PinguScene newState, PinguSceneData scene, object data)
        {
            switch (newState)
            {
                case PinguScene.Menu:
                    MusicMachine.Singleton.Play(menuMusic);
                    break;
                case PinguScene.Splash:
                    break;
                case PinguScene.World:
                    MusicMachine.Singleton.Play(((WorldAsset)data ?? WorldSceneParameter.World).Music);
                    break;
                case PinguScene.Game:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(newState), newState, null);
            }

            PointerInputSystem.Singleton.UpdateCamera(Camera.main);
        }
    }

    public enum PinguScene
    {
        Splash,
        Menu,
        World,
        Game,
    }

    [Serializable]
    public sealed class PinguSceneData : SceneData<PinguScene>
    {
        public PinguSceneData(PinguScene identifier) : base(identifier)
        {
        }

        public PinguSceneData(PinguScene identifier, string[] scenes) : base(identifier, scenes)
        {
        }
    }
}