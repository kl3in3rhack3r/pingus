using System;
using System.Collections;
using System.Collections.Generic;
using PcSoft.ExtendedUnity._90_Scripts._00_Runtime.Components;
using UnityEngine;
using UnityEngine.InputSystem;

namespace _00_Main._90_Scripts._00_Runtime.Components.Tooling
{
    [AddComponentMenu(PingusConstants.Menu.Components.ToolingMenu + "/Pointer Input System")]
    [DisallowMultipleComponent]
    public sealed class PointerInputSystem : SearchingSingletonBehavior<PointerInputSystem>
    {
        #region Inspector Data

        [Header("Physics")]
        [SerializeField]
        private LayerMask layer = int.MaxValue;

        #endregion
        
        private Collider2D _colliderOver = null;
        private Camera _camera;
        private bool _hasCamera;

        #region Builtin Methods

        private void Update()
        {
            if (!_hasCamera)
                return;
            
            var hit = Physics2D.Raycast(_camera.ScreenToWorldPoint(Pointer.current.position.ReadValue()), Vector2.zero, float.MaxValue, layer);
            if (hit.collider != null)
            {
                if (Mouse.current.leftButton.wasPressedThisFrame)
                {
                    HandlePointer(hit.collider, ph => ph.OnPointerPress());
                }
                else
                {
                    if (_colliderOver == null)
                    {
                        HandlePointer(hit.collider, ph => ph.OnPointerEnter());
                        _colliderOver = hit.collider;
                    }
                }
            }
            else if (_colliderOver != null)
            {
                HandlePointer(_colliderOver, ph => ph.OnPointerExit());
                _colliderOver = null;
            }
        }

        #endregion

        public void UpdateCamera(Camera camera)
        {
            Debug.Log("Update camera: " + (camera?.ToString() ?? "<no camera>"));
            
            _camera = camera;
            _hasCamera = _camera != null;
        }

        public void ClearCamera()
        {
            UpdateCamera(null);
        }

        private static void HandlePointer(Collider2D collider, Action<IPointerHandler> handler)
        {
            foreach (var pointerHandler in collider.GetComponents<IPointerHandler>())
            {
                handler(pointerHandler);
            }
        }
    }

    public interface IPointerHandler
    {
        void OnPointerEnter();
        void OnPointerExit();
        void OnPointerPress();
    }
}