using System;
using PcSoft.ExtendedAnimation._90_Scripts._00_Runtime.Utils;
using PcSoft.UnityBlending._90_Scripts._00_Runtime;
using UnityEngine;

namespace _00_Main._90_Scripts._00_Runtime.Components.Tooling
{
    [AddComponentMenu(PingusConstants.Menu.Components.ToolingMenu + "/Blending System")]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(CanvasGroup))]
    [DefaultExecutionOrder(-1)]
    public sealed class PinguBlendingSystem : BlendingSystem
    {
        #region Inspector Data

        [Header("Animation")]
        [SerializeField]
        private AnimationCurve fadingCurve = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

        [SerializeField]
        private float fadingSpeed = 1f;

        [Header("References")]
        [SerializeField]
        private Camera camera;

        #endregion

        #region Properties

        public override float LoadingProgress { get; set; }

        #endregion

        private CanvasGroup _canvasGroup;

        #region Builtin Methods

        private void Awake()
        {
            _canvasGroup = GetComponent<CanvasGroup>();
        }

        #endregion

        public override void ShowBlend(Action onFinished = null)
        {
            _canvasGroup.blocksRaycasts = true;
            AnimationBuilder.Create(this)
                .Animate(fadingCurve, fadingSpeed, v => _canvasGroup.alpha = v)
                .WithFinisher(() =>
                {
                    camera.gameObject.SetActive(true);
                    onFinished?.Invoke();
                })
                .Start();
        }

        public override void ShowBlendImmediately()
        {
            _canvasGroup.alpha = 1f;
            _canvasGroup.blocksRaycasts = true;
            camera.gameObject.SetActive(true);
        }

        public override void HideBlend(Action onFinished = null)
        {
            AnimationBuilder.Create(this)
                .Animate(fadingCurve, fadingSpeed, v => _canvasGroup.alpha = 1f - v)
                .WithFinisher(() =>
                {
                    _canvasGroup.blocksRaycasts = false;
                    camera.gameObject.SetActive(false);
                    onFinished?.Invoke();
                })
                .Start();
        }

        public override void HideBlendImmediately()
        {
            _canvasGroup.alpha = 0f;
            _canvasGroup.blocksRaycasts = false;
            camera.gameObject.SetActive(false);
        }
    }
}