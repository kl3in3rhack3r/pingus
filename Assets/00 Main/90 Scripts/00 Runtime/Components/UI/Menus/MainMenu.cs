
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
#if UNITY_EDITOR
#endif

namespace _00_Main._90_Scripts._00_Runtime.Components.UI.Menus
{
    [AddComponentMenu(PingusConstants.Menu.Components.UI.MenuMenu + "/Main Menu")]
    [DisallowMultipleComponent]
    public sealed class MainMenu : UIBehaviour
    {
        public void HandleExit()
        {
#if UNITY_EDITOR
            EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
    }
}