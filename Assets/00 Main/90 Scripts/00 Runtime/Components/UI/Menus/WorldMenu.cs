using _00_Main._90_Scripts._00_Runtime.Components.Tooling;
using _00_Main._90_Scripts._00_Runtime.Components.Tooling.Parameters;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace _00_Main._90_Scripts._00_Runtime.Components.UI.Menus
{
    [AddComponentMenu(PingusConstants.Menu.Components.UI.MenuMenu + "/World Menu")]
    [DisallowMultipleComponent]
    public sealed class WorldMenu : UIBehaviour
    {
        #region Inspector Data

        [Header("References")]
        [SerializeField]
        private Text lblWorldName;

        #endregion

        #region Builtin Methods

        protected override void OnEnable()
        {
            lblWorldName.text = WorldSceneParameter.World.Title;
        }

        #endregion

        public void HandleBack()
        {
            PinguSceneSystem.Singleton.GotoMenu();
        }
    }
}