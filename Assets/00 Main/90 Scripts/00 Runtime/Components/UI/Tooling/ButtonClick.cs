using _00_Main._90_Scripts._00_Runtime.Assets.Commons.Presets;
using PcSoft.AudioMachine._90_Scripts._00_Runtime.Components.Sfx;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace _00_Main._90_Scripts._00_Runtime.Components.UI.Tooling
{
    [AddComponentMenu(PingusConstants.Menu.Components.UI.ToolingMenu + "/Button Click")]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Button))]
    public sealed class ButtonClick : UIBehaviour
    {
        private Button _button;

        #region Builtin Methods

        protected override void Awake()
        {
            _button = GetComponent<Button>();
        }

        protected override void OnEnable()
        {
            _button.onClick.AddListener(OnClick);
        }

        protected override void OnDisable()
        {
            _button.onClick.RemoveListener(OnClick);
        }

        #endregion

        private void OnClick()
        {
            SfxMachine.Singleton.Play(UiSfxPreset.Singleton.Click);
        }
    }
}