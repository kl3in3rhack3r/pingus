using System;
using PcSoft.UnityCommons._90_Scripts._00_Runtime.Utils.Extensions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace _00_Main._90_Scripts._00_Runtime.Components.UI.Tooling
{
    [AddComponentMenu(PingusConstants.Menu.Components.UI.ToolingMenu + "/Image Texture Shifting")]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(RawImage))]
    public class ImageTextureShifting : UIBehaviour
    {
        #region Inspector Data

        [Header("Animation")]
        [SerializeField]
        private Vector2 speed = Vector2.zero;

        #endregion

        private RawImage _image;

        #region Builtin Methods

        protected override void Awake()
        {
            _image = GetComponent<RawImage>();
        }

        private void FixedUpdate()
        {
            _image.uvRect = _image.uvRect.UpdatePosition(_image.uvRect.position + speed * Time.fixedDeltaTime);
        }

        #endregion
    }
}