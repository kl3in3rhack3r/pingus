using System;
using _00_Main._90_Scripts._00_Runtime.Assets.World;
using _00_Main._90_Scripts._00_Runtime.Components.Tooling;
using PcSoft.ExtendedUI._90_Scripts._00_Runtime.Components.UI.Component;
using UnityEngine;
using UnityEngine.UI;

namespace _00_Main._90_Scripts._00_Runtime.Components.UI.Components.Lists
{
    [AddComponentMenu(PingusConstants.Menu.Components.UI.Component.ListMenu + "/World Item List")]
    [DisallowMultipleComponent]
    public sealed class WorldListItem : UiListItem<WorldAsset>
    {
        #region Inspector Data

        [Header("References")]
        [SerializeField]
        private Text lblTitle;

        [SerializeField]
        private Text lblDescription;

        [SerializeField]
        private Image imgIcon;

        #endregion

        public void OpenWorld()
        {
            PinguSceneSystem.Singleton.GotoWorld(Model);
        }

        protected override void OnAttachModel(WorldAsset model)
        {
            lblTitle.text = model.Title;
            lblDescription.text = model.Description;
            imgIcon.sprite = model.Icon;
        }
    }
}