using _00_Main._90_Scripts._00_Runtime.Assets.World;
using PcSoft.ExtendedUI._90_Scripts._00_Runtime.Components.UI.Component;
using UnityEngine;

namespace _00_Main._90_Scripts._00_Runtime.Components.UI.Components.Lists
{
    [AddComponentMenu(PingusConstants.Menu.Components.UI.Component.ListMenu + "/World List")]
    [DisallowMultipleComponent]
    public class WorldList : UiList<WorldListItem, WorldAsset>
    {
        protected override WorldAsset[] ContentData => WorldAsset.All;
    }
}