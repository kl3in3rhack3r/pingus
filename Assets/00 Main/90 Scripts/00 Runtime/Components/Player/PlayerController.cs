using UnityEngine;
using UnityEngine.Serialization;

namespace _00_Main._90_Scripts._00_Runtime.Components.Player
{
    public abstract class PlayerController : MonoBehaviour
    {
        #region Inspector Data

        [FormerlySerializedAs("walkSpeed")]
        [Header("Animation")]
        [SerializeField]
        protected float walkingSpeed = 1f;

        #endregion
    }
}