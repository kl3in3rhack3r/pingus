using System;
using PcSoft.ExtendedAnimation._90_Scripts._00_Runtime.Utils;
using UnityEngine;

namespace _00_Main._90_Scripts._00_Runtime.Components.Player
{
    [AddComponentMenu(PingusConstants.Menu.Components.PlayerMenu + "/World Player Controller")]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Animator))]
    public sealed class WorldPlayerController : PlayerController
    {
        #region Static Area

        public static WorldPlayerController Singleton => Resources.FindObjectsOfTypeAll<WorldPlayerController>()[0];

        #endregion
        
        private Animator _animator;

        #region Builtin Methods

        private void Awake()
        {
            _animator = GetComponent<Animator>();
        }

        #endregion

        public void WalkTo(Vector3 target, Action onFinished = null)
        {
            StopAllCoroutines();
            
            Debug.Log("Walk pingu to " + target, this);
            
            var startPos = transform.position;
            var distance = Vector3.Distance(startPos, target);
            var forward = (target - startPos).x > 0f;

            _animator.SetFloat("speed", forward ? walkingSpeed : -walkingSpeed);
            AnimationBuilder.Create(this)
                .Animate(AnimationCurve.Linear(0f, 0f, 1f, 1f), distance / walkingSpeed,
                    v => transform.position = Vector3.Lerp(startPos, target, v))
                .WithFinisher(() =>
                {
                    _animator.SetFloat("speed", 0f);
                    onFinished?.Invoke();
                })
                .Start();
        }
    }
}