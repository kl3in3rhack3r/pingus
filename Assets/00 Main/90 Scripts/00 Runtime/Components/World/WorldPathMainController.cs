using System;
using _00_Main._90_Scripts._00_Runtime.Components.Player;
using UnityEngine;

namespace _00_Main._90_Scripts._00_Runtime.Components.World
{
    [AddComponentMenu(PingusConstants.Menu.Components.WorldMenu + "/Path Controller/Main Path")]
    [DisallowMultipleComponent]
    public sealed class WorldPathMainController : WorldPathController
    {
        #region Inspector Data

        [Header("Player")]
        [SerializeField]
        private WorldPlayerController player;

        #endregion

        #region Properties

#if UNITY_EDITOR
        protected override Color GizmosColor { get; } = Color.green;
#endif

        #endregion

        #region Builtin Methods

        protected override void OnEnable()
        {
            base.OnEnable();
            Instantiate(player.gameObject, points[0].Point.transform.position + relativePosition, Quaternion.identity);
        }

        #endregion

        
    }
}