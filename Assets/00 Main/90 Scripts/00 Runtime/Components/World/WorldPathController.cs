using System;
using System.Linq;
using _00_Main._90_Scripts._00_Runtime.Components.Player;
using UnityEngine;

namespace _00_Main._90_Scripts._00_Runtime.Components.World
{
    public abstract class WorldPathController : MonoBehaviour
    {
        #region Inspector Data
        
        [Header("Positioning")]
        [SerializeField]
        protected Vector3 relativePosition = Vector3.zero;

        [Header("References")]
        [SerializeField]
        protected WorldPoint[] points;

        #endregion

        #region Properties

        internal WorldPoint[] Points => points;

#if UNITY_EDITOR
        protected abstract Color GizmosColor { get; }
#endif

        #endregion
        
        private readonly PlayerPosition _playerPosition = new PlayerPosition();

        #region Builtin Methods

        protected virtual void OnEnable()
        {
            if (points.Length <= 0)
                throw new InvalidOperationException("No points defined");
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            if (points == null || points.Length < 2)
                return;
            
            Gizmos.color = GizmosColor;
            
            for (var i = 1; i < points.Length; i++)
            {
                if (points[i - 1].SubPaths == null || points[i - 1].SubPaths.Length < 1)
                {
                    Gizmos.DrawLine(points[i - 1].Point.transform.position, points[i].Point.transform.position);
                }
                else
                {
                    foreach (var subPath in points[i - 1].SubPaths)
                    {
                        if (subPath.points == null || subPath.points.Length < 1)
                            continue;

                        Gizmos.DrawLine(points[i - 1].Point.transform.position, subPath.points.First().Point.transform.position);
                        Gizmos.DrawLine(subPath.points.Last().Point.transform.position, points[i].Point.transform.position);
                    }
                }
            }
        }
#endif

        #endregion
        
        public void WalkTo(WorldPointController pointController)
        {
            var targetPosition = FindPoint(pointController, points);
            if (targetPosition == null)
                throw new InvalidOperationException("Unable to find given path point");

            if (_playerPosition.Index == targetPosition.Index)
            {
                _playerPosition.Update(targetPosition);
                WalkOnSubPath(pointController);
            }
            else
            {
                var forward = _playerPosition.IsForwardDirectionRelativeTo(targetPosition);
                var nextIndex = forward ? _playerPosition.Index + 1 : _playerPosition.Index - 1;
                WalkToNext(nextIndex, targetPosition.Index, forward, () =>
                {
                    _playerPosition.Update(targetPosition);
                    WalkOnSubPath(pointController);
                });
            }
        }

        private void WalkToNext(int nextIndex, int targetIndex, bool forward, Action onFinished)
        {
            WorldPlayerController.Singleton.WalkTo(points[nextIndex].Point.transform.position + relativePosition, () =>
            {
                var index = forward ? nextIndex + 1 : nextIndex - 1;
                if (nextIndex == targetIndex)
                {
                    onFinished?.Invoke();
                }
                else
                {
                    WalkToNext(index, targetIndex, forward, onFinished);
                }
            });
        }

        private void WalkOnSubPath(WorldPointController controller)
        {
            if (_playerPosition.SubPath == null)
                return;
            
            _playerPosition.SubPath.WalkTo(controller);
        }

        private static PlayerPosition FindPoint(WorldPointController pointController, WorldPoint[] points)
        {
            for (var i = 0; i < points.Length; i++)
            {
                if (points[i].Point == pointController)
                    return new PlayerPosition(i, null); //Found point on this path

                foreach (var subPath in points[i].SubPaths)
                {
                    var point = FindPoint(pointController, subPath.Points);
                    if (point != null)
                        return new PlayerPosition(i, subPath); //point was found on sub path
                }
            }

            return null; //No fit point was found
        }

        private sealed class PlayerPosition
        {
            public int Index { get; private set; } = 0;
            public WorldPathSubController SubPath { get; private set; } = null;

            public PlayerPosition()
            {
            }

            public PlayerPosition(int index, WorldPathSubController subPath = null)
            {
                Index = index;
                SubPath = subPath;
            }

            public void Update(int index, WorldPathSubController subPath = null)
            {
                Index = index;
                SubPath = subPath;
            }

            public void Update(PlayerPosition p)
            {
                Update(p.Index, p.SubPath);
            }

            public bool IsForwardDirectionRelativeTo(PlayerPosition p)
            {
                return Index < p.Index;
            }

            private bool Equals(PlayerPosition other)
            {
                return Index == other.Index && Equals(SubPath, other.SubPath);
            }

            public override bool Equals(object obj)
            {
                return ReferenceEquals(this, obj) || obj is PlayerPosition other && Equals(other);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    return (Index * 397) ^ (SubPath != null ? SubPath.GetHashCode() : 0);
                }
            }

            public static bool operator ==(PlayerPosition p1, PlayerPosition p2)
            {
                return Equals(p1, p2);
            }

            public static bool operator !=(PlayerPosition p1, PlayerPosition p2)
            {
                return !Equals(p1, p2);
            }
        }
    }

    [Serializable]
    public sealed class WorldPoint
    {
        #region Inspector Data

        [SerializeField]
        private WorldPointController point;

        [SerializeField]
        private WorldPathSubController[] subPaths;

        #endregion

        #region Properties

        public WorldPointController Point => point;

        public WorldPathSubController[] SubPaths => subPaths;

        #endregion
    }
}