using System;
using _00_Main._90_Scripts._00_Runtime.Assets.Commons;
using _00_Main._90_Scripts._00_Runtime.Assets.Commons.Presets;
using _00_Main._90_Scripts._00_Runtime.Assets.World;
using _00_Main._90_Scripts._00_Runtime.Components.Tooling;
using _00_Main._90_Scripts._00_Runtime.Storage;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace _00_Main._90_Scripts._00_Runtime.Components.World
{
    [AddComponentMenu(PingusConstants.Menu.Components.WorldMenu + "/Point Controller/Level Point")]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(SpriteRenderer), typeof(Collider2D))]
    public sealed class WorldPointLevelController : WorldPointController, IPointerHandler
    {
        #region Inspector Data

        [Header("References")]
        [SerializeField]
        private WorldAsset world;
        
        [SerializeField]
        private LevelAsset level;

        #endregion

        private bool LevelAccessible => level.AlwaysUnlocked || StoryStorage.CanAccessLevel(world.Identifier, level.Identifier);
        
        private SpriteRenderer _renderer;
        private WorldPathMainController _pathController;

        #region Builtin Methods

        private void Awake()
        {
            _renderer = GetComponent<SpriteRenderer>();
            _pathController = GetComponentInParent<WorldPathMainController>();
            if (_pathController == null)
                throw new InvalidOperationException("No path controller for this level point");
        }

        private void OnEnable()
        {
            _renderer.sprite = LevelAccessible ? LevelPointPreset.Singleton.Unlocked.Regularly : LevelPointPreset.Singleton.Locked.Regularly;
        }

        #endregion

        public void OnPointerEnter()
        {
            Debug.Log("Highlighting level point", this);
            _renderer.sprite = LevelAccessible ? LevelPointPreset.Singleton.Unlocked.Highlighted : LevelPointPreset.Singleton.Locked.Highlighted;
        }

        public void OnPointerExit()
        {
            Debug.Log("Regularly level point", this);
            _renderer.sprite = LevelAccessible ? LevelPointPreset.Singleton.Unlocked.Regularly : LevelPointPreset.Singleton.Locked.Regularly;
        }

        public void OnPointerPress()
        {
            Debug.Log("Walk pingu to level point", this);
            _pathController.WalkTo(this);
        }
    }
}