using UnityEngine;

namespace _00_Main._90_Scripts._00_Runtime.Components.World
{
    [AddComponentMenu(PingusConstants.Menu.Components.WorldMenu + "/Path Controller/Sub Path")]
    [DisallowMultipleComponent]
    public sealed class WorldPathSubController : WorldPathController
    {
        #region Properties

#if UNITY_EDITOR
        protected override Color GizmosColor { get; } = Color.yellow;
#endif

        #endregion
    }
}