using System;
using UnityEngine;

namespace _00_Main._90_Scripts._00_Runtime.Components.World
{
    [AddComponentMenu(PingusConstants.Menu.Components.WorldMenu + "/Point Controller/Empty Point")]
    [DisallowMultipleComponent]
    public sealed class WorldPointEmptyController : WorldPointController
    {
        #region Builtin Methods

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(transform.position, 0.07f);
        }
#endif

        #endregion
    }
}