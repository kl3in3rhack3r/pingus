using System;
using _00_Main._90_Scripts._00_Runtime.Components.Tooling;
using PcSoft.ExtendedAnimation._90_Scripts._00_Runtime.Utils;
using PcSoft.UnityCommons._90_Scripts._00_Runtime.Utils.Extensions;
using UnityEngine;

namespace _00_Main._90_Scripts._00_Runtime.Components.World
{
    [AddComponentMenu(PingusConstants.Menu.Components.WorldMenu + "/Overlay Handler")]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(SpriteRenderer), typeof(Collider2D))]
    public sealed class WorldOverlayHandler : MonoBehaviour, IPointerHandler
    {
        #region Inspector Data

        [Header("Animation")]
        [SerializeField]
        private AnimationCurve fadingCurve = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

        [SerializeField]
        private float fadingSpeed = 1f;
        
        [SerializeField]
        [Range(0f, 1f)]
        private float defaultAlpha = 1f;

        [SerializeField]
        [Range(0f, 1f)]
        private float overlayAlpha = 0.5f;

        [SerializeField]
        [Range(0f, 1f)]
        private float hidingAlpha = 0f;

        #endregion
        
        private SpriteRenderer _renderer;
        private Color _originalColor;
        private bool _playerInside = false;

        #region Builtin Methods

        private void Awake()
        {
            _renderer = GetComponent<SpriteRenderer>();
            
            _originalColor = _renderer.color;
            _renderer.color = _originalColor.UpdateAlpha(defaultAlpha);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                Debug.Log("Player enter area", this);
                
                _playerInside = true;
                AnimateFading(_renderer.color.a, hidingAlpha);
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                Debug.Log("Player leave area", this);
                
                _playerInside = false;
                AnimateFading(hidingAlpha, defaultAlpha);
            }
        }

        #endregion
        
        public void OnPointerEnter()
        {
            if (_playerInside)
                return;
            
            AnimateFading(defaultAlpha, overlayAlpha);
        }

        public void OnPointerExit()
        {
            if (_playerInside)
                return;
            
            AnimateFading(overlayAlpha, defaultAlpha);
        }

        public void OnPointerPress()
        {
        }

        private void AnimateFading(float fromAlpha, float toAlpha)
        {
            StopAllCoroutines();

            var startColor = _originalColor.UpdateAlpha(fromAlpha);
            var endColor = _originalColor.UpdateAlpha(toAlpha);
            AnimationBuilder.Create(this)
                .Animate(fadingCurve, fadingSpeed, v => _renderer.color = Color.Lerp(startColor, endColor, v))
                .Start();
        }
    }
}