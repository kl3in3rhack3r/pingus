using PcSoft.SavePrefs._90_Scripts._00_Runtime.Utils;
using UnityEngine;

namespace _00_Main._90_Scripts._00_Runtime.Storage
{
    public static class StoryStorage
    {
        private const string BaseKey = "story.";
        private const string WorldBaseKey = "world.";
        private const string LevelBaseKey = "level.";
        
        private static string GetWorldBaseKey(string id) => BaseKey + WorldBaseKey + id;
        private static string GetLevelBaseKey(string worldId, string levelId) => GetWorldBaseKey(worldId) + LevelBaseKey + levelId;

        public static bool CanAccessWorld(string id) => PlayerPrefsEx.GetBool(GetWorldBaseKey(id), false);
        public static void AccessWorld(string id) => PlayerPrefsEx.SetBool(GetWorldBaseKey(id), true, true);

        public static bool CanAccessLevel(string worldId, string levelId) => PlayerPrefsEx.GetBool(GetLevelBaseKey(worldId, levelId), false);
        public static void AccessLevel(string worldId, string levelId) => PlayerPrefsEx.SetBool(GetLevelBaseKey(worldId, levelId), true, true);
    }
}