namespace _00_Main._90_Scripts._00_Runtime
{
    internal static class PingusConstants
    {
        private const string Root = "Pingus";

        public static class Menu
        {
            public static class Assets
            {
                public const string CommonMenu = Root + "/Commons";
                public const string WorldMenu = Root + "/World";

                public static class Common
                {
                    public const string PresetMenu = CommonMenu + "/Presets";
                }
            }

            public static class Components
            {
                public const string ToolingMenu = Root + "/Tooling";
                public const string PlayerMenu = Root + "/Player";
                public const string WorldMenu = Root + "/World";

                private const string UIMenu = Root + "/UI";

                public static class Tooling
                {
                    public const string ParameterMenu = ToolingMenu + "/Parameters";
                }

                public static class UI
                {
                    public const string ToolingMenu = UIMenu + "/Tooling";
                    public const string ComponentMenu = UIMenu + "/Components";
                    public const string DialogMenu = UIMenu + "/Dialogs";
                    public const string MenuMenu = UIMenu + "/Menus";

                    public static class Component
                    {
                        public const string ListMenu = ComponentMenu + "/Lists";
                    }
                }
            }
        }

        public static class Path
        {
            public static class Resource
            {
                public const string CommonPath = "Commons";
                public const string WorldPath = "Worlds";

                public static class Common
                {
                    public const string PresetPath = CommonPath + "/Presets";
                }
            }
        }
    }
}
