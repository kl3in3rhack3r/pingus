using _00_Main._90_Scripts._00_Runtime.Assets.Commons;
using _00_Main._90_Scripts._00_Runtime.Assets.Commons.Presets;
using _00_Main._90_Scripts._00_Runtime.Assets.World;
using PcSoft.DynamicAssets._90_Scripts._00_Runtime.Loader;
using UnityEngine;

namespace _00_Main._90_Scripts._00_Runtime
{
    public static class PingusEvents
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSplashScreen)]
        public static void Init()
        {
            Debug.Log("Load common presets...");
            AssetResourcesLoader.Instance.LoadAssets<LevelPointPreset>(PingusConstants.Path.Resource.Common.PresetPath);
            AssetResourcesLoader.Instance.LoadAssets<UiSfxPreset>(PingusConstants.Path.Resource.Common.PresetPath);
            
            Debug.Log("Load worlds and levels...");
            AssetResourcesLoader.Instance.LoadAssets<WorldAsset>(PingusConstants.Path.Resource.WorldPath);
        }
    }
}