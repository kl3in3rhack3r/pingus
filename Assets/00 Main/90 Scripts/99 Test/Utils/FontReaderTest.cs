using _00_Main._90_Scripts._90_Editor.Utils;
using NUnit.Framework;
using UnityEngine;

namespace _00_Main._90_Scripts._99_Test.Utils
{
    public class FontReaderTest
    {
        [Test]
        public void TestReadSimple()
        {
            var fontData = FontReader.Read("(glyph (unicode 506) (offset 0 -19) (advance 12) (rect 0 0 14 23))");
            Assert.IsTrue(fontData.HasValue);
            Assert.AreEqual(506, fontData.Value.Unicode);
            Assert.AreEqual(new Vector2(0, -19), fontData.Value.Offset);
            Assert.AreEqual(12, fontData.Value.Advance);
            Assert.AreEqual(Rect.MinMaxRect(0, 0, 14, 23), fontData.Value.Rect);
        }
        
        [Test]
        public void TestReadInline()
        {
            var fontData = FontReader.Read("    (glyph (unicode 506) (offset 0 -19) (advance 12) (rect 0 0 14 23))");
            Assert.IsTrue(fontData.HasValue);
            Assert.AreEqual(506, fontData.Value.Unicode);
            Assert.AreEqual(new Vector2(0, -19), fontData.Value.Offset);
            Assert.AreEqual(12, fontData.Value.Advance);
            Assert.AreEqual(Rect.MinMaxRect(0, 0, 14, 23), fontData.Value.Rect);
        }
        
        [Test]
        public void TestReadNegative()
        {
            var fontData = FontReader.Read("(glyph (unicode 506) (offset -1 -19) (advance 12) (rect 100 100 114 123))");
            Assert.IsTrue(fontData.HasValue);
            Assert.AreEqual(506, fontData.Value.Unicode);
            Assert.AreEqual(new Vector2(-1, -19), fontData.Value.Offset);
            Assert.AreEqual(12, fontData.Value.Advance);
            Assert.AreEqual(Rect.MinMaxRect(100, 100, 114, 123), fontData.Value.Rect);
        }
        
        [Test]
        public void TestReadFailure()
        {
            var fontData = FontReader.Read("(<>glyph (unicode 506) (offset 0 -19) (advance 12) (rect 0 0 14 23))");
            Assert.IsFalse(fontData.HasValue);
        }
    }
}