using UnityEngine;

namespace PcSoft.UnityCommons._90_Scripts._00_Runtime.Utils.Extensions
{
    public static class ColorExtensions
    {
        public static Color UpdateAlpha(this Color color, float a)
        {
            return new Color(color.r, color.g, color.b, a);
        }
    }
}