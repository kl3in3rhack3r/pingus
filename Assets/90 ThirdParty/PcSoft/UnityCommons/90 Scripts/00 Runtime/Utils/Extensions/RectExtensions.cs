using System.Runtime.InteropServices;
using UnityEngine;

namespace PcSoft.UnityCommons._90_Scripts._00_Runtime.Utils.Extensions
{
    public static class RectExtensions
    {
        public static Rect UpdatePosition(this Rect rect, Vector2 position)
        {
            return UpdatePosition(rect, position.x, position.y);
        }
        
        public static Rect UpdatePosition(this Rect rect, float x, float y)
        {
            return new Rect(x, y, rect.width, rect.height);
        }
    }
}